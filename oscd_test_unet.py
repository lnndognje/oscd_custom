# Laurent NL. inference script on DSMSCN dataset using supervised trained model

import os
import cv2 as cv
import pickle
import numpy as np
from keras.models import load_model
from keras.optimizers import Adam

from acc_util import Recall, Precision, F1_score
from seg_model.U_net.FC_Siam_Diff import get_FCSD_model
from seg_model.U_net.FC_Siam_Conc import get_FCSC_model
from seg_model.U_net.FC_EF import get_FCEF_model
from acc_ass import accuracy_assessment
from net_util import weight_binary_cross_entropy
from scipy import misc

parser = argparse.ArgumentParser()
parser.add_argument('--max_epoch', type=int, default=200, help='epoch to run[default: 100]')
parser.add_argument('--batch_size', type=int, default=1, help='batch size during training[default: 512]')
parser.add_argument('--learning_rate', type=float, default=2e-4, help='initial learning rate[default: 3e-4]')
parser.add_argument('--result_save_path', default='./result', help='model param path')
parser.add_argument('--model_save_path', default='./model_param', help='model param path')
parser.add_argument('--data_path', default='data', help='data path')
parser.add_argument('--data_set_name', default='ACD/Szada', help='dataset name')
parser.add_argument('--gpu_num', type=int, default=1, help='number of GPU to train')

# basic params
FLAGS = parser.parse_args()

BATCH_SZ = FLAGS.batch_size
LEARNING_RATE = FLAGS.learning_rate
MAX_EPOCH = FLAGS.max_epoch
RESULT_SAVE_PATH = FLAGS.result_save_path
MODEL_SAVE_PATH = FLAGS.model_save_path
DATA_PATH = FLAGS.data_path
DATA_SET_NAME = FLAGS.data_set_name
GPU_NUM = FLAGS.gpu_num
BATCH_PER_GPU = BATCH_SZ // GPU_NUM
#
print('Parametrisation done...')

# read dataset

def read_data():
    path = './data/ACD/Szada/test'
    # check if pickle files already exists; if it's the case, delete them and recreate them.
    if os.path.exists('data/Szada/test_sample_1.pickle'): os.remove('data/Szada/test_sample_1.pickle')
    if os.path.exists('data/Szada/test_sample_2.pickle'): os.remove('data/Szada/test_sample_2.pickle')
    if os.path.exists('data/Szada/test_label.pickle'): os.remove('data/Szada/test_label.pickle')
    train_img_1 = []
    train_img_2 = []
    train_label = []
    file_names = sorted(os.listdir(path))
    for file_name in file_names:
        if file_name[-4:].upper() == '.BMP':
            img = cv.imread(os.path.join(path, file_name))
            if img.shape[0] > img.shape[1]:
                img = img[0:784, :, :]
            elif img.shape[0] < img.shape[1]:
                img = img[:, 0:784, :]
            if 'gt.bmp' in file_name.lower():
                img = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
                train_label.append(img)
            elif 'im1.bmp' in file_name.lower():
                train_img_1.append(img)
            elif 'im2.bmp' in file_name.lower():
                train_img_2.append(img)
    with open('data/Szada/test_sample_1.pickle', 'wb') as file:
        pickle.dump(train_img_1, file)
    with open('data/Szada/test_sample_2.pickle', 'wb') as file:
        pickle.dump(train_img_2, file)
    with open('data/Szada/test_label.pickle', 'wb') as file:
        pickle.dump(train_label, file)
    #
    print('Reading data ok')

# 
# load model

def test_model():
    result_path = os.path.join(RESULT_SAVE_PATH, DATA_SET_NAME)
    model_save_path = os.path.join(MODEL_SAVE_PATH, DATA_SET_NAME)
    if not os.path.exists(result_path):
        os.makedirs(result_path)
    if not os.path.exists(model_save_path):
        os.makedirs(model_save_path)

    test_X, test_Y, test_label = get_data()
    
    # load model
    deep_model = load_model(os.path.join(model_save_path, str(34) + '_model.h5'))
    # loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    # summarize model.
    deep_model.summary()
    # load dataset
    # dataset = loadtxt("pima-indians-diabetes.csv", delimiter=",")
    # split into input (X) and output (Y) variables
    # X = dataset[:,0:8]
    # Y = dataset[:,8]
    # evaluate the model
    # score = model.evaluate(X, Y, verbose=0)
    if True:
        loss, acc, sen, spe, F1 = deep_model.evaluate(x=[test_X, test_Y], y=test_label, batch_size=1)
        # loss, acc, sen, spe, F1 = deep_model.evaluate(x=test, y=test_label, batch_size=1) # FC-EF

        binary_change_map = deep_model.predict([test_X, test_Y])
        #  binary_change_map = deep_model.predict(test)

        binary_change_map = np.reshape(binary_change_map, (binary_change_map.shape[1], binary_change_map.shape[2]))
        idx_1 = binary_change_map > 0.5
        idx_2 = binary_change_map <= 0.5
        binary_change_map[idx_1] = 255
        binary_change_map[idx_2] = 0

        conf_mat, overall_acc, kappa = accuracy_assessment(
            gt_changed=np.reshape(255 * test_label, (test_label.shape[1], test_label.shape[2])),
            gt_unchanged=np.reshape(255. - 255 * test_label, (test_label.shape[1], test_label.shape[2])),
            changed_map=binary_change_map)

        info = 'Test loss is %.4f,  test sen is %.4f, test spe is %.4f, test F1 is %.4f, test acc is %.4f, ' \
               'test kappa is %.4f, ' % (loss, sen, spe, F1, overall_acc, kappa)
        print(info)
        print('confusion matrix is ', conf_mat)
        # test process complete
        #
        with open(os.path.join(result_path, 'log_test_0.txt'), 'a+') as f:
            f.write(info + '\n')
            cv.imwrite(os.path.join(result_path, str(34) + '_test_bcm.bmp'), binary_change_map)

def get_data():
    path = os.path.join(DATA_PATH, DATA_SET_NAME)
    test_X, test_Y, test_label = load_test_data(path=path)

    test_X = np.array(test_X) / 255.
    test_Y = np.array(test_Y) / 255.
    test_label = np.array(test_label) / 255.
    test_label = np.reshape(test_label, (test_label.shape[0], test_label.shape[1], test_label.shape[2]))
    

    return test_X, test_Y, test_label



def load_test_data(path):
    with open(os.path.join(path, 'test_sample_1.pickle'), 'rb') as file:
        test_X = pickle.load(file)
    with open(os.path.join(path, 'test_sample_2.pickle'), 'rb') as file:
        test_Y = pickle.load(file)
    with open(os.path.join(path, 'test_label.pickle'), 'rb') as file:
        test_label = pickle.load(file)

    return test_X, test_Y, test_label


if __name__ == '__main__':
    read_data()
    #
    test_model()