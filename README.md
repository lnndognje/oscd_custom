# OSCD_Custom

Project to test several change detection algorithms on the OSCD data set composed of Sentinel-2 imagery

Fully convolutional network architectures for change detection using remote sensing images.

[Rodrigo Caye Daudt, Bertrand Le Saux, Alexandre Boulch. (2018, October). Fully convolutional siamese networks for change detection. In 2018 25th IEEE International Conference on Image Processing (ICIP) (pp. 4063-4067). IEEE.](https://ieeexplore.ieee.org/abstract/document/8451652)

[arXiv](https://arxiv.org/abs/1810.08462)